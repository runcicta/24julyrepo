// typeid, typeinfo
/*
*ANSI-C++ also defines a new operator called typeid that allows checking the type of an expression
*this operator returns a refernece to a constant object of type type_info that is defined in the standard header file <typeinfo>.
*This returned value can be compared with another using operators == / != or can serve to obtain a string of characters representing the data type or class name by using its name() method.
*/
#include <iostream>
#include <typeinfo>

struct CDummy { };

int main() {
	CDummy* a, *b;
	if (typeid(a) != typeid(b))
	{
		std::cout << "a and b are of different types:\n";
		std::cout << "a is: " << typeid(a).name() << '\n';
		std::cout << "b is: " << typeid(b).name() << '\n';
	}
	return 0;
}