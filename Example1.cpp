//Which one compiles successfully ? Explain.

struct Foo {};
struct Bar {};

int main(int argc, char** argv)
{
	Foo* f = new Foo;

	Bar* b1 = f;                              // (1) cannot assign foo pointer to a bar pointer without cast, because they are unrelated structures
	Bar* b2 = static_cast<Bar*>(f);           // (2) can perform convesion only to pointers of related structs
	Bar* b3 = dynamic_cast<Bar*>(f);          // (3) dynamic cast can only be used wth pointers and references to objects
	Bar* b4 = reinterpret_cast<Bar*>(f);      // (4) transforsm any type to any type is OK
	Bar* b5 = const_cast<Bar*>(f);            // (5) this only changes the constant status

	return 0;
}